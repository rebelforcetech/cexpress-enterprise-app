
function authenticate() {
  const token = sessionStorage.getItem("accessToken");
  const orderPath = `http://localhost:3000/api/orders?access_token=${token}`;
  console.log(token);
  getOrderData(token, orderPath);

}


function getOrderData(token, orderPath) {
  var data = null;

  var xhr = new XMLHttpRequest();
  xhr.withCredentials = true;

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      // console.log(this.responseText);
      var orderData = JSON.parse(this.responseText);
      console.log(orderData);
      loadOrders(orderData);


    }
  });

  xhr.open("GET", `${orderPath}`);
  // xhr.setRequestHeader("cache-control", "no-cache");
  // xhr.setRequestHeader("postman-token", "9bebd2d9-1cb0-f42f-f688-5c792f583ebb");
  xhr.setRequestHeader("accessToken", token)

  xhr.send(data);
}

function loadOrders(orderData) {
    var orderContainer = document.getElementById('orderTableBody');
    orderContainer.innerHTML = orderData.map(orderTemplate).join("");
}

function orderTemplate(order) {
  return `
  <tr>
    <td>
      ${order.id}
    </td>

    <td>
      ${order.itemName}
    </td>
    <td>
      ${order.itemWeight}
    </td>
    <td class="text-primary">
      ${order.userID}
    </td>
    <td class="text-primary">
        12
    </td>
    <td>
    <a href="#" class="icon whatsapp-bg"><i class="fa fa-whatsapp"></i></a>
    <a href="#" class="icon google-bg"><i class="fa fa-google"></i></a>

    </td>
  </tr>
  `
}
