function dashTemplate() {
return `
 <div class="container-fluid">
   <div class="row">
     <div class="col-lg-3 col-md-6 col-sm-6">
       <div class="card card-stats">
         <div class="card-header card-header-warning card-header-icon">
           <div class="card-icon">
             <i class="material-icons">content_copy</i>
           </div>
           <p class="card-category"> Pending Orders</p>
           <h3 class="card-title">49
             <small>orders</small>
           </h3>
         </div>
         <div class="card-footer">
           <div class="stats">
             <i class="material-icons text-danger">warning</i>
             <a href="#pablo">Manage Pending...</a>
           </div>
         </div>
       </div>
     </div>
     <div class="col-lg-3 col-md-6 col-sm-6">
       <div class="card card-stats">
         <div class="card-header card-header-success card-header-icon">
           <div class="card-icon">
             <i class="material-icons">store</i>
           </div>
           <p class="card-category">At Warehouse
           </p>
           <h3 class="card-title">34
                 <small>orders</small>
           </h3>
         </div>
         <div class="card-footer">

         </div>
       </div>
     </div>
     <div class="col-lg-3 col-md-6 col-sm-6">
       <div class="card card-stats">
         <div class="card-header card-header-warning card-header-icon">
           <div class="card-icon">
             <i class="fa fa-truck"></i>
           </div>
           <p class="card-category">Orders In Transit</p>
           <h3 class="card-title">75 <small>orders</small></h3>
         </div>
         <div class="card-footer">

         </div>
       </div>
     </div>
     <div class="col-lg-3 col-md-6 col-sm-6">
       <div class="card card-stats">
         <div class="card-header card-header-success card-header-icon">
           <div class="card-icon">
             <i class="fa fa-check"></i>
           </div>
           <p class="card-category">Delivered Today</p>
           <h3 class="card-title">20 <small>Orders</small></h3>
         </div>
         <div class="card-footer">

         </div>
       </div>
     </div>
   </div>
   <div class="row">
     <div class="col-md-6">
       <div class="card card-chart">
         <div class="card-header card-header-success">
           <div class="ct-chart" id="dailySalesChart"></div>
         </div>
         <div class="card-body">
           <h4 class="card-title">Lifetime Sales</h4>
           <p class="card-category">
             <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p>
         </div>
         <div class="card-footer">
           <div class="stats">
             <i class="material-icons">access_time</i> updated 4 minutes ago
           </div>
         </div>
       </div>
     </div>
     <div class="col-md-6">
       <div class="card card-chart">
         <div class="card-header card-header-warning">
           <div class="ct-chart" id="websiteViewsChart"></div>
         </div>
         <div class="card-body">
           <h4 class="card-title">New Users</h4>
           <p class="card-category">Last Campaign Performance</p>
         </div>
         <div class="card-footer">
         </div>
       </div>
     </div>

   </div>

     <div class="col-lg-12 col-md-12">
         <div class="card">
           <div class="card-header card-header-warning">
             <h4 class="card-title">Employees Stats</h4>
             <p class="card-category">New employees on 15th September, 2016</p>
           </div>
           <div class="card-body table-responsive">
             <table class="table table-hover">
               <thead class="text-warning">
                 <tr><th>UserID</th>
                 <th>Item Name</th>
                 <th>COD</th>
                 <th>Delivery Location</th>
               </tr></thead>
               <tbody>
                 <tr>
                   <td>1</td>
                   <td>1KG Rice x 2</td>
                   <td>৳36,738</td>
                   <td>Dhanmondi, Dhaka</td>
                 </tr>
                 <tr>
                   <td>2</td>
                   <td>Donuts</td>
                   <td>৳3,789</td>
                   <td>Dhanmondi, Dhaka</td>
                 </tr>
                 <tr>
                   <td>3</td>
                   <td>Guitar</td>
                   <td>৳56,142</td>
                   <td>Dhanmondi, Dhaka</td>
                 </tr>
                 <tr>
                   <td>4</td>
                   <td>Item Item</td>
                   <td>৳38,735</td>
                   <td>Dhanmondi, Dhaka</td>
                 </tr>
               </tbody>
             </table>
           </div>
         </div>
       </div>
   </div>
 </div>
`;


}

function getDashData() {

}
