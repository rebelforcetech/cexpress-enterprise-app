
$(document).ready(function() {
// check log in

var token = localStorage.getItem("accessToken");
// console.log(token);

if (token === null) {
console.log('not logged in');
window.location="/index-login.html"
}

//initialize footermenu
  var footermenu = document.getElementsByClassName('footer-menu');
  footermenu.innerHTML = menuTemplate();
});



function logout() {
var token = localStorage.getItem("accessToken");
var data = JSON.stringify({"userID": token});
var xhr = new XMLHttpRequest();
  xhr.withCredentials = false;
  xhr.addEventListener("readystatechange", function () {if (this.readyState === 4) {console.log(this.responseText);}});

  xhr.open("POST", "https://cexpress.bubbleapps.io/version-test/api/1.1/wf/logout");
  xhr.setRequestHeader("content-type", "application/json");
  xhr.send(data);


localStorage.removeItem("accessToken");
window.location = "/login.html"

}



function menuTemplate() {
return `
<a href="store-faq.html" class="footer-menu-item">
<em>Cart</em><i class="scale-hover gold fa fa-shopping-cart"></i>
</a>

<a href="store-faq.html" class="footer-menu-item">
<em>Chat</em><i class="scale-hover gold fa fa-comment"></i>
</a>

<a href="store-checkout.html" class="footer-menu-item">
<em>Log Out</em><i class="scale-hover gold fa fa-sign-out"></i>
</a>
`;
}
